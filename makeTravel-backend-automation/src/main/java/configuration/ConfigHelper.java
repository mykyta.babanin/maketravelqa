package configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Getter
@Component
@PropertySource("classpath:${env:local}/application.properties")
public class ConfigHelper {

    @Value("${database.host}")
    private String databaseHost;

    @Value("${database.username}")
    private String databaseUsername;

    @Value("${database.password}")
    private String databasePassword;

    @Value("${sdg.url}")
    public String url;

    @Value("${sdg.security.url}")
    public String securityUrl;

    @Value("${default.username}")
    public String username;

    @Value("${default.password}")
    public String password;
}
